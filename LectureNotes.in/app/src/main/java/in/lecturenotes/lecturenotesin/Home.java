package in.lecturenotes.lecturenotesin;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Home extends AppCompatActivity {
    String url = "http://www.lecturenotes.in";
    int count = 0;
    private SwipeRefreshLayout swipeLayout;
    private WebView wv1;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeLayout.setRefreshing(false);
                        wv1.loadUrl(wv1.getUrl());
                    }
                }, 2000);
            }
        });
        wv1 = (WebView) findViewById(R.id.webView);
        wv1.setWebViewClient(new MyBrowser());
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.getSettings().setDomStorageEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv1.loadUrl(url);

        progress = ProgressDialog.show(Home.this, "LectureNotes.in", "Loading your Homepage");

        wv1.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                if (progress.isShowing()) {
                    progress.dismiss();
                }
            }
            public WebResourceResponse shouldInterceptRequest (final WebView view, WebResourceRequest request) {
                return super.shouldInterceptRequest(view, request);
            }
        });
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(url);
            wv1.post(new Runnable() {

                @Override
                public void run() {
                    wv1.loadUrl(url);
                }
            });
            return true;
        }
    }

    public void onBackPressed() {
        if (wv1.canGoBack()) {
                wv1.goBack();
        } else {
            count++;
            if(count == 2)
                finish();
            else
                Toast.makeText(this, "Press again to exit LectureNotes.in", Toast.LENGTH_SHORT).show();
        }
    }
}